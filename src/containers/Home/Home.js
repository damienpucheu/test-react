import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { fetch } from "./actions";
import Grid from "material-ui/Grid";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";

class Home extends React.PureComponent {
  componentDidMount() {
    this.props.dispatch(fetch());
  }

  renderPostsList = () => {
    return this.props.posts.map(post => {
      return (
        <Grid item md={4}>
          <Card key={post.id}>
            <CardContent>{post.title}</CardContent>
            <CardActions>
              <CardActions>
                <Link to={`/posts/${post.id}`}>
                  <Button size="small">Learn more</Button>
                </Link>
              </CardActions>
            </CardActions>
          </Card>
        </Grid>
      );
    });
  };

  render() {
    return (
      <div>
        <p>This is the home page</p>
        <ul>
          <Grid container spacing={16}>
            {this.renderPostsList()}
          </Grid>
        </ul>
      </div>
    );
  }
}

export default connect(({ home }) => ({
  posts: home.posts
}))(Home);
