import Api from '../../utils/Api'

export const FETCHED_POST = 'FETCHED_POST';
export const FETCHED_USER = 'FETCHED_USER';
export const FETCHED_COMMENTS = 'FETCHED_COMMENTS';
export const START_FETCH = 'START_FETCH';

export function fetchedPost(post) {
    return {
        type: FETCHED_POST,
        post
    }
}

export function fetchedUser(user) {
    return {
        type: FETCHED_USER,
        user
    }
}

export function fetchedComments(comments) {
    return {
        type: FETCHED_COMMENTS,
        comments
    }
}

export function startFetch() {
    return {
        type: START_FETCH
    }
}

export function fetch(postId) {
    return (dispatch) => {

        dispatch(startFetch());

        Api.getPost(postId).then(data => {
            dispatch(fetchedPost(data));
        }).catch(err => {
            console.error(err);
        });

        Api.getUser(postId).then(data => {
            dispatch(fetchedUser(data));
        }).catch(err => {
            console.error(err);
        });

        Api.getPostComments(postId).then(data => {
            dispatch(fetchedComments(data));
        }).catch(err => {
            console.error(err);
        });

    }
}
